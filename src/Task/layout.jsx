import React from 'react';
import './assets/style.css';


const Layout = () => {
    return (
        <div className="body">
            <header>
                <h1 contenteditable>Header.com</h1>
            </header>
            <div className="left-sidebar" contenteditable>Left Sidebar</div>
            <main contenteditable></main>
            <div className="right-sidebar" contenteditable>Right Sidebar</div>
            <footer contenteditable>Footer Content — Header.com 2020</footer>
        </div>
    );
}

export default Layout;